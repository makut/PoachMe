﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PoachMe.Models;
using Swashbuckle.AspNetCore.Swagger;
using Serilog;

namespace PoachMe
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddEntityFrameworkNpgsql().AddDbContext<PoachMeContext>(opt => opt.UseNpgsql(Configuration.GetConnectionString("PoachMeContext")));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "PoachMe API",
                    Description = "API for PoachMe Services",
                    TermsOfService = "None",
                    Contact = new Contact
                    {
                        Name = "Dorcas Maku Tamatey",
                        Email = "dorcas@hubtel.com",
                        Url = "https://twitter.com/mtamatey"
                    },
                    License = new License
                    {
                        Name = "None"

                    },

                });
                //c.IncludeXmlComments(string.Format(@"{0}Dromi.xml",
                //         AppDomain.CurrentDomain.BaseDirectory));
                // var filePath = Path.Combine(System.AppContext.BaseDirectory, "Dromi.xml");
                // c.IncludeXmlComments(filePath);

            });
            services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "PoachMe API V1");
            });

            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseStaticFiles();
        }
    }
}
