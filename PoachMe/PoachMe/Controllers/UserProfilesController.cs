﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PoachMe.Commons;
using PoachMe.Models;

namespace PoachMe.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfilesController : ControllerBase
    {
        private readonly PoachMeContext _context;
        

        public UserProfilesController(PoachMeContext context)
        {
            _context = context;
        }

        // GET: api/UserProfiles
        [HttpGet]
        public IEnumerable<UserProfile> GetProfile()
        {
            return _context.Profile;
        }
        [HttpGet("{industryId}")]
        public IEnumerable<UserProfile> GetProfileByIndustry(int id)
        {
            // var profile = _context.Industry.FindAsync(id);
            var profile = _context.Profile;
            var profilesByIndustries = from c in profile where c.IndustryId == id select c;
            return profilesByIndustries;
        }
        // GET: api/UserProfiles/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserProfile([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");

                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<UserProfile>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            var userProfile = await _context.Profile.FindAsync(id);

            if (userProfile == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<UserProfile>()
                {
                    Message = " item is not found",
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }
            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<UserProfile>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE

            }); 
        }

        // PUT: api/UserProfiles/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserProfile([FromRoute] int id, [FromBody] UserProfile userProfile)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");

                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<UserProfile>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            if (id != userProfile.UserProfileId)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<UserProfile>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.Entry(userProfile).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserProfileExists(id))
                {
                    var message = ModelState.GetErrorMessages(",");
                    return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<UserProfile>()
                    {
                        Message = message,
                        Status = CommonStatusCode.STATUS_CODE_FAILED

                    });
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UserProfiles
        [HttpPost]
        public async Task<IActionResult> PostUserProfile([FromBody] UserProfile userProfile)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<UserProfile>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.Profile.Add(userProfile);
            await _context.SaveChangesAsync();

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<UserProfile>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE

            });
        }

        // DELETE: api/UserProfiles/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserProfile([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<UserProfile>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            var userProfile = await _context.Profile.FindAsync(id);
            if (userProfile == null)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<UserProfile>()
                {
                    Message = "item not found",
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.Profile.Remove(userProfile);
            await _context.SaveChangesAsync();

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<UserProfile>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS

            });
        }
        [HttpGet("{id}")]
        private bool UserProfileExists(int id)
        {
            return _context.Profile.Any(e => e.UserProfileId == id);
        }
    }
}