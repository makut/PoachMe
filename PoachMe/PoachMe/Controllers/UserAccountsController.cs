﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PoachMe.Commons;
using PoachMe.Models;

namespace PoachMe.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAccountsController : ControllerBase
    {
        private readonly PoachMeContext _context;
        

        public UserAccountsController(PoachMeContext context)
        {
            _context = context;
        }

        // GET: api/UserAccounts
        [HttpGet]
        public IEnumerable<UserAccount> GetAccount()
        {
            return _context.Account;
        }

        // GET: api/UserAccounts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserAccount([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<UserAccount>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            var userAccount = await _context.Account.FindAsync(id);

            if (userAccount == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<UserAccount>()
                {
                    Message = "item is not found",
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<UserAccount>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE

            });
        }

        // PUT: api/UserAccounts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserAccount([FromRoute] int id, [FromBody] UserAccount userAccount)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<UserAccount>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            if (id != userAccount.UserAccountId)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<UserAccount>()
                {
                    Message = "id is not valid",
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.Entry(userAccount).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserAccountExists(id))
                {
                    return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<UserAccount>()
                    {
                        Message = CommonStatusCode.NOT_FOUND_ERROR_MESSAGE,
                        Status = CommonStatusCode.STATUS_CODE_FAILED

                    });
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UserAccounts
        [HttpPost]
        public async Task<IActionResult> PostUserAccount([FromBody] UserAccount userAccount)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<UserAccount>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.Account.Add(userAccount);
            await _context.SaveChangesAsync();

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<UserAccount>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS

            });
        }

        // DELETE: api/UserAccounts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserAccount([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userAccount = await _context.Account.FindAsync(id);
            if (userAccount == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<UserAccount>()
                {
                    Message = CommonStatusCode.NOT_FOUND_ERROR_MESSAGE,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.Account.Remove(userAccount);
            await _context.SaveChangesAsync();

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<UserAccount>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE

            });
        }
        [HttpGet("{id}")]
        private bool UserAccountExists(int id)
        {
            return _context.Account.Any(e => e.UserAccountId == id);
        }
    }
}