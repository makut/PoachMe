﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PoachMe.Commons;
using PoachMe.Models;

namespace PoachMe.Controllers
{
    [Route("industries")]
    [ApiController]
    public class IndustriesController : ControllerBase
    {
        private readonly PoachMeContext _context;

        public IndustriesController(PoachMeContext context)
        {
            _context = context;
        }

        // GET: api/Industries
        [HttpGet]
        public IEnumerable<Industry> GetIndustry()
        {
            return _context.Industry;
        }

        // GET: api/Industries/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetIndustry([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<JobListings>()
                {
                    Message =message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            var industry = await _context.Industry.FindAsync(id);

            if (industry == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<JobListings>()
                {
                    Message = "id is not correct",
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });

            }

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<JobListings>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS
               ,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE

            });
        }

        // PUT: api/Industries/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutIndustry([FromRoute] int id, [FromBody] Industry industry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != industry.IndustryId)
            {
                return BadRequest();
            }

            _context.Entry(industry).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IndustryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Industries
        [HttpPost]
        public async Task<IActionResult> PostIndustry([FromBody] Industry industry)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<JobListings>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.Industry.Add(industry);
            await _context.SaveChangesAsync();

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<JobListings>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE

            });
        }

        // DELETE: api/Industries/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteIndustry([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<JobListings>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            var industry = await _context.Industry.FindAsync(id);
            if (industry == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<JobListings>()
                {
                    Message = "Item not found",
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.Industry.Remove(industry);
            await _context.SaveChangesAsync();

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<JobListings>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS

            });
        }
        [HttpGet("{id}")]
        private bool IndustryExists(int id)
        {
            return _context.Industry.Any(e => e.IndustryId == id);
        }
    }
}