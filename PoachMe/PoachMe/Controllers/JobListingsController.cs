﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PoachMe.Commons;
using PoachMe.Models;

namespace PoachMe.Controllers
{
    [Route("joblistings")]
    [ApiController]
    public class JobListingsController : ControllerBase
    {
        private readonly PoachMeContext _context;

        public JobListingsController(PoachMeContext context)
        {
            _context = context;
        }

        // GET: api/JobListings
        [HttpGet]
        public IEnumerable<JobListings> GetJobListings()
        {
            return _context.JobListings;
        }

        // GET: api/JobListings/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetJobListings([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<JobListings>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            var jobListings = await _context.JobListings.FindAsync(id);

            if (jobListings == null)
            {
            
                return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<JobListings>()
                {
                    Message = "Id is not valid",
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            return Ok(jobListings);
        }

        // PUT: api/JobListings/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutJobListings([FromRoute] int id, [FromBody] JobListings jobListings)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<JobListings>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            if (id != jobListings.JobListingsId)
            {
              
                return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<JobListings>()
                {
                    Message = "Id is not found",
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.Entry(jobListings).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobListingsExists(id))
                {
                    
                    return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<JobListings>()
                    {
                        Message = "item does not exist",
                        Status = CommonStatusCode.STATUS_CODE_FAILED

                    });
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/JobListings
        [HttpPost]
        public async Task<IActionResult> PostJobListings([FromBody] JobListings jobListings)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<JobListings>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.JobListings.Add(jobListings);
            await _context.SaveChangesAsync();

   
            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<JobListings>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE

            });
        }

        // DELETE: api/JobListings/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteJobListings([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<JobListings>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            var jobListings = await _context.JobListings.FindAsync(id);
            if (jobListings == null)
            {
                
                return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<JobListings>()
                {
                    Message = " item is not available",
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.JobListings.Remove(jobListings);
            await _context.SaveChangesAsync();

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<JobListings>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE

            });
        }

        private bool JobListingsExists(int id)
        {
            return _context.JobListings.Any(e => e.JobListingsId == id);
        }
    }
}