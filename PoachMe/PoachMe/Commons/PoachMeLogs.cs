﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoachMe.Commons
{
    public class PoachMeLogs
    {
        public static void Info<S>(string template, params object[] param)
        {
            Log.ForContext<S>().Information(template, param);
        }
        public static void Warning<S>(string template, params object[] param)
        {
            Log.ForContext<S>().Warning(template, param);
        }
        public static void Debug<S>(string template, params object[] param)
        {
            Log.ForContext<S>().Debug(template, param);
        }
        public static void Fatal<S>(Exception exception, string template, params object[] param)
        {
            Log.ForContext<S>().Fatal(template, param);
        }
        public static void Error<S>(Exception exception, string template, params object[] param)
        {
            Log.ForContext<S>().Error(template, param);
        }
    }
}
