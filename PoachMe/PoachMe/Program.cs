﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;

namespace PoachMe
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);
            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            var config = new ConfigurationBuilder().AddCommandLine(args).Build();
            return WebHost.CreateDefaultBuilder(args).UseContentRoot(Directory.GetCurrentDirectory())
                .UseConfiguration(config).UseKestrel().UseIISIntegration().UseStartup<Startup>().UseSerilog((ctx,
                    cfg) => cfg.ReadFrom.Configuration(ctx.Configuration).Enrich.FromLogContext()).Build();
        }
    }
}
