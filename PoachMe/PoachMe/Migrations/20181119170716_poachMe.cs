﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace PoachMe.Migrations
{
    public partial class poachMe : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    UserAccountId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.UserAccountId);
                });

            migrationBuilder.CreateTable(
                name: "Company",
                columns: table => new
                {
                    CompanyId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IndustryId = table.Column<int>(nullable: false),
                    CompanyName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company", x => x.CompanyId);
                });

            migrationBuilder.CreateTable(
                name: "Industry",
                columns: table => new
                {
                    IndustryId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IndustryName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Industry", x => x.IndustryId);
                });

            migrationBuilder.CreateTable(
                name: "JobListings",
                columns: table => new
                {
                    JobListingsId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CompanyId = table.Column<int>(nullable: false),
                    ContactEmail = table.Column<string>(nullable: true),
                    ContactNumber = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Introduction = table.Column<string>(nullable: true),
                    JobDescription = table.Column<string>(nullable: true),
                    qualification = table.Column<string>(nullable: true),
                    Certification = table.Column<string>(nullable: true),
                    YearsOfExperience = table.Column<string>(nullable: true),
                    Level = table.Column<string>(nullable: true),
                    DeadLine = table.Column<string>(nullable: true),
                    ModeOfSubmission = table.Column<string>(nullable: true),
                    DisplayCompanyName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobListings", x => x.JobListingsId);
                });

            migrationBuilder.CreateTable(
                name: "Profile",
                columns: table => new
                {
                    UserProfileId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    UserAccountId = table.Column<int>(nullable: false),
                    WorKExp = table.Column<string>(nullable: true),
                    IndustryId = table.Column<int>(nullable: false),
                    Specialty = table.Column<string>(nullable: true),
                    Certifications = table.Column<string>(nullable: true),
                    Qualifications = table.Column<string>(nullable: true),
                    PreviousCompany = table.Column<string>(nullable: true),
                    CurrentCompany = table.Column<string>(nullable: true),
                    ProjectHistory = table.Column<string>(nullable: true),
                    FullTime = table.Column<string>(nullable: true),
                    PartTime = table.Column<string>(nullable: true),
                    Contract = table.Column<string>(nullable: true),
                    Project = table.Column<string>(nullable: true),
                    Nationality = table.Column<string>(nullable: true),
                    CountryOfResidence = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    OverseesRelocation = table.Column<string>(nullable: true),
                    InCountryRelocation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profile", x => x.UserProfileId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "Company");

            migrationBuilder.DropTable(
                name: "Industry");

            migrationBuilder.DropTable(
                name: "JobListings");

            migrationBuilder.DropTable(
                name: "Profile");
        }
    }
}
