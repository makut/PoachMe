﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace PoachMe.Models
{
    public static class ModelStateExtensions
    {
        public static string GetErrorMessages(this ModelStateDictionary model, string separator)
        {
            var errorMessage = "";

            var error = model.ErrorCount;
            if (error > 0)
            {
                foreach (var errorFound in model.Values)
                {
                    errorMessage = string.Join(separator ?? "\r\n", errorFound.Errors.Select(e => e.ErrorMessage));
                }

            }

            return errorMessage;

        }
    }
}

