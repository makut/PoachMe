﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoachMe.Models
{
    public class UserProfile
    {
        public int UserProfileId { get; set; }
        public int UserAccountId { get; set; }
        public string WorKExp { get; set; }
        public int IndustryId { get; set; }
        public string Specialty { get; set; }
        public string Certifications { get; set; }
        public string Qualifications { get; set; }
        public string PreviousCompany { get; set; }
        public string CurrentCompany { get; set; }
        public string ProjectHistory { get; set; }
        public string FullTime { get; set; }
        public string PartTime { get; set; }
        public string Contract { get; set; }
        public string Project { get; set; }
        public string Nationality { get; set; }
        public string CountryOfResidence { get; set; }
        public string Region { get; set; }
        public string OverseesRelocation { get; set; }
        public string InCountryRelocation { get; set; }
    }
}
