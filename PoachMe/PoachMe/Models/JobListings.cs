﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoachMe.Models
{
    public class JobListings
    {
        public int JobListingsId { get; set; }
        public int CompanyId { get; set; }
        public string ContactEmail { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string Introduction { get; set; }
        public string JobDescription { get; set; }
        public string qualification { get; set; }
        public string Certification { get; set; }
        public string YearsOfExperience { get; set; }
        public string Level { get; set; }
        public string DeadLine { get; set; }
        public string ModeOfSubmission { get; set; }
        public string DisplayCompanyName { get; set; }



    }
}
