﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using PoachMe.Models;

namespace PoachMe.Models
{
    public class PoachMeContext : DbContext
    {


        public PoachMeContext(DbContextOptions<PoachMeContext> options)
                : base(options)
        {
        }

        //public DromiContext(DromiContext context)
        //{
        //    _context = context;
        //}

        public DbSet<UserAccount> Account { get; set; }
        public DbSet<UserProfile> Profile { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<Industry> Industry { get; set; }
        public DbSet<PoachMe.Models.JobListings> JobListings { get; set; }

    }
}